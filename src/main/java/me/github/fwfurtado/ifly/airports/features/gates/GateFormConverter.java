package me.github.fwfurtado.ifly.airports.features.gates;

import me.github.fwfurtado.ifly.airports.domain.Gate;
import me.github.fwfurtado.ifly.airports.features.gates.AddGateController.GateForm;
import me.github.fwfurtado.ifly.airports.infra.Mapper;
import org.springframework.stereotype.Component;

import static me.github.fwfurtado.ifly.airports.infra.StringNormalizer.normalize;

@Component
class GateFormConverter implements Mapper<GateForm, Gate> {
    @Override
    public Gate convert(GateForm source) {
        return new Gate(normalize(source.getGate()));
    }
}
