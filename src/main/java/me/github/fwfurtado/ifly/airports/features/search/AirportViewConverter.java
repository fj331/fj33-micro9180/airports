package me.github.fwfurtado.ifly.airports.features.search;

import me.github.fwfurtado.ifly.airports.domain.Airport;
import me.github.fwfurtado.ifly.airports.infra.Mapper;
import org.springframework.stereotype.Component;

import static me.github.fwfurtado.ifly.airports.features.search.SearchController.*;

@Component
class AirportViewConverter implements Mapper<Airport, AirportView> {
    @Override
    public AirportView convert(Airport source) {
        return new AirportView(source.getIdentifier(), source.getLocation());
    }
}
