package me.github.fwfurtado.ifly.airports.infra;

import java.text.Normalizer;

public class StringNormalizer {
    public static String normalize(String source) {
        return Normalizer.normalize(source.toUpperCase(), Normalizer.Form.NFD)
        .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}
