package me.github.fwfurtado.ifly.airports.features.creation;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.github.fwfurtado.ifly.airports.domain.Location;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.*;

@RestController
@RequestMapping("airports")
@AllArgsConstructor
class CreationAirportController {

    private final CreationAirportService service;

    @PostMapping
    ResponseEntity<?> createBy(@RequestBody @Valid AirportForm form, UriComponentsBuilder uriBuilder) {
        var identifier = service.createNewAirportFrom(form);

        var uri = uriBuilder.path("/airports/{id}").build(identifier);

        return created(uri).build();

    }

    @Data
    static class AirportForm {
        private String code;
        private Location location;
    }
}
