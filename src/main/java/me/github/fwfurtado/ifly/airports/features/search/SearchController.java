package me.github.fwfurtado.ifly.airports.features.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.github.fwfurtado.ifly.airports.domain.Airport;
import me.github.fwfurtado.ifly.airports.domain.Location;
import me.github.fwfurtado.ifly.airports.features.search.SearchRepository.SiteView;
import me.github.fwfurtado.ifly.airports.infra.Mapper;
import me.github.fwfurtado.ifly.airports.infra.ResourceNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;
import java.util.List;

import static me.github.fwfurtado.ifly.airports.infra.StringNormalizer.normalize;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("airports")
@AllArgsConstructor
class SearchController {

    private final AirportViewConverter airportConverter;
    private final SiteViewConverter siteConverter;
    private final SearchRepository repository;

    @GetMapping("{identifier}")
    AirportView showBy(@PathVariable String identifier) {
        return findAirportAndConvertWith(identifier, airportConverter);
    }

    @GetMapping("{identifier}/sites")
    List<SiteView> showSitesBy(@PathVariable String identifier) {
        return findAirportAndConvertWith(identifier, siteConverter);
    }

    @GetMapping("{airportIdentifier}/sites/{siteIdentifier}")
    SiteView exist(@PathVariable String airportIdentifier, @PathVariable String siteIdentifier) {
        return repository.existsSiteBy(normalize(airportIdentifier), normalize(siteIdentifier)).orElseThrow(() -> new ResourceNotFoundException("Cannot find site"));
    }

    private <T> T findAirportAndConvertWith(String identifier, Mapper<Airport, T> converter) {
        var message = MessageFormat.format("Cannot find airport with code %s", identifier);
        return repository.findByIdentifier(normalize(identifier)).map(converter::convert).orElseThrow(() -> new ResourceNotFoundException(message));
    }

    @Getter
    @AllArgsConstructor
    static class SiteViewImpl implements SiteView {
        private String airport;
        private String gate;
    }

    @Getter
    @AllArgsConstructor
    static class AirportView {
        private String identifier;
        private Location location;
    }
}
