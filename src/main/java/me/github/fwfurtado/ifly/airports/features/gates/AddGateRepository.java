package me.github.fwfurtado.ifly.airports.features.gates;

import me.github.fwfurtado.ifly.airports.domain.Airport;

import java.util.Optional;

public interface AddGateRepository {
    Optional<Airport> findByIdentifier(String identifier);

    void save(Airport airport);
}
