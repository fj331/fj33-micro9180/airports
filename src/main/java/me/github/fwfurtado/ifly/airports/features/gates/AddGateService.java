package me.github.fwfurtado.ifly.airports.features.gates;

import lombok.AllArgsConstructor;
import me.github.fwfurtado.ifly.airports.infra.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

import static me.github.fwfurtado.ifly.airports.features.gates.AddGateController.GateForm;

@Service
@AllArgsConstructor
class AddGateService {

    private final AddGateRepository repository;
    private GateFormConverter converter;

    public void addGateBy(GateForm form) {
        var identifier = form.getIdentifier();
        var exceptionMessage = MessageFormat.format("Cannot find airport with the identifier %s", identifier);

        var airport = repository.findByIdentifier(identifier).orElseThrow(() -> new ResourceNotFoundException(exceptionMessage));

        var gate = converter.convert(form);

        airport.addGate(gate);

        repository.save(airport);
    }
}

