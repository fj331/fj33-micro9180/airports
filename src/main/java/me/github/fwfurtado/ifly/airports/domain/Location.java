package me.github.fwfurtado.ifly.airports.domain;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Embeddable
public class Location {
    private String state;
    private String city;
    private String neighborhood;

    public static void main(String[] args) {
        System.out.println(LocalDateTime.now());
    }
}
