package me.github.fwfurtado.ifly.airports.features;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@AllArgsConstructor
public class HystrixController {

    private final HystrixService service;

    @RequestMapping("hystrix")
    Map<String, String> circuitBreaker(@RequestParam Long sessionId) throws InterruptedException {
        return service.run(sessionId);
    }
}
