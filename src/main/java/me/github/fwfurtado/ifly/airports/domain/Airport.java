package me.github.fwfurtado.ifly.airports.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "airports")
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
public class Airport {
    @Id
    @NonNull
    private String identifier;

    @NotNull
    @Embedded
    @NonNull
    private Location location;

    @ElementCollection
    private Set<Gate> gates = new HashSet<>();

    public String getIdentifier() {
        return identifier;
    }

    public void addGate(Gate gate) {
        gates.add(gate);
    }
}
