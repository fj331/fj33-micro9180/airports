package me.github.fwfurtado.ifly.airports.features.gates;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import static me.github.fwfurtado.ifly.airports.infra.StringNormalizer.normalize;
import static org.springframework.http.ResponseEntity.accepted;

@RestController
@RequestMapping("airports")
@AllArgsConstructor
class AddGateController {

    private final AddGateService service;

    @PutMapping("{identifier}/gates")
    ResponseEntity<?> addGateTo(@PathVariable String identifier, @RequestBody @Valid GateForm form) {
        form.setIdentifier(normalize(identifier));

        service.addGateBy(form);

        return accepted().build();
    }

    @Data
    static class GateForm {
        @JsonIgnore
        private String identifier;

        @NotBlank
        private String gate;
    }
}
