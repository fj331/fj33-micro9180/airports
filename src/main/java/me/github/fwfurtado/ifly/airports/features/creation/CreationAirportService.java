package me.github.fwfurtado.ifly.airports.features.creation;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.github.fwfurtado.ifly.airports.configuration.Channels;
import me.github.fwfurtado.ifly.airports.domain.Airport;
import me.github.fwfurtado.ifly.airports.domain.Location;
import me.github.fwfurtado.ifly.airports.features.creation.CreationAirportController.AirportForm;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
class CreationAirportService {

    private final Channels source;
    private final AirportFormConverter converter;
    private final CreateAirportRepository repository;


    public String createNewAirportFrom(AirportForm form) {
        var airport = converter.convert(form);

        repository.save(airport);

        var event = MessageBuilder.withPayload(new AirportCreated(airport)).build();
        source.airports().send(event);

        return airport.getIdentifier();
    }

    @Data
    static class AirportCreated {
        private final LocalDateTime time = LocalDateTime.now();
        private String identifier;
        private Location location;

        public AirportCreated(Airport airport) {
            this.identifier = airport.getIdentifier();
            this.location = airport.getLocation();
        }
    }
}
