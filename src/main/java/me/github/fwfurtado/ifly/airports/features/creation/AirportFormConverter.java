package me.github.fwfurtado.ifly.airports.features.creation;

import me.github.fwfurtado.ifly.airports.features.creation.CreationAirportController.AirportForm;
import me.github.fwfurtado.ifly.airports.domain.Airport;
import me.github.fwfurtado.ifly.airports.infra.Mapper;
import org.springframework.stereotype.Component;

import static me.github.fwfurtado.ifly.airports.infra.StringNormalizer.normalize;

@Component
class AirportFormConverter implements Mapper<AirportForm, Airport> {

    @Override
    public Airport convert(AirportForm source) {
        return new Airport(normalize(source.getCode()), source.getLocation());
    }
}
