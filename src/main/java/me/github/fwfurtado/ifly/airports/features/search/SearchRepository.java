package me.github.fwfurtado.ifly.airports.features.search;

import me.github.fwfurtado.ifly.airports.domain.Airport;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface SearchRepository {
    Optional<Airport> findByIdentifier(String identifier);

    @Query("select a.identifier as airport, g.identifier as gate from Airport a join a.gates g where a.identifier = :identifier and g.identifier = :siteIdentifier")
    Optional<SiteView> existsSiteBy(String identifier, String siteIdentifier);

    interface SiteView {
        String getAirport();
        String getGate();
    }

}
