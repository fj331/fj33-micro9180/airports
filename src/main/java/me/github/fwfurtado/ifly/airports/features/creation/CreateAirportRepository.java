package me.github.fwfurtado.ifly.airports.features.creation;

import me.github.fwfurtado.ifly.airports.domain.Airport;

public interface CreateAirportRepository {
    void save(Airport airport);
}
