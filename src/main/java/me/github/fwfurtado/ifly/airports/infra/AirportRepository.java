package me.github.fwfurtado.ifly.airports.infra;

import me.github.fwfurtado.ifly.airports.features.creation.CreateAirportRepository;
import me.github.fwfurtado.ifly.airports.domain.Airport;
import me.github.fwfurtado.ifly.airports.features.gates.AddGateRepository;
import me.github.fwfurtado.ifly.airports.features.search.SearchRepository;
import org.springframework.data.repository.Repository;

interface AirportRepository extends Repository<Airport, Long>, CreateAirportRepository, SearchRepository, AddGateRepository {

}
