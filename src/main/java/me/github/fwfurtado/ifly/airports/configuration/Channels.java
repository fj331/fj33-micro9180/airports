package me.github.fwfurtado.ifly.airports.configuration;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface Channels {

    @Output("airports")
    MessageChannel airports();
}
