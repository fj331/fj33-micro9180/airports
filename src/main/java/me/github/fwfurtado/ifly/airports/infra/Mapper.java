package me.github.fwfurtado.ifly.airports.infra;

public interface Mapper<S, T> {
    T convert(S source);
}
