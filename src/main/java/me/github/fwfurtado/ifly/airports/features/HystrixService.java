package me.github.fwfurtado.ifly.airports.features;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import me.github.fwfurtado.ifly.airports.infra.ResourceNotFoundException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class HystrixService {

    @HystrixCommand(fallbackMethod = "fallback")
    public Map<String, String> run(Long sessionId)  {

        if(sessionId % 2 == 0) {
            throw new ResourceNotFoundException("Cannot find session");
        }

        return Map.of("status", "OK");
    }

    public Map<String, String> fallback(Long sessionId) {
        return Map.of("status", "FAIL", "sessionId", sessionId.toString());
    }
}
