package me.github.fwfurtado.ifly.airports.features.search;

import me.github.fwfurtado.ifly.airports.domain.Airport;
import me.github.fwfurtado.ifly.airports.infra.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static me.github.fwfurtado.ifly.airports.features.search.SearchController.SiteViewImpl;
import static me.github.fwfurtado.ifly.airports.features.search.SearchRepository.*;

@Component
class SiteViewConverter implements Mapper<Airport, List<SiteView>> {
    @Override
    public List<SiteView> convert(Airport source) {
        var identifier = source.getIdentifier();
        return source.getGates().stream().map(gate -> new SiteViewImpl(identifier, gate.getIdentifier())).collect(Collectors.toList());
    }
}
