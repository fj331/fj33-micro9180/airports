package me.github.fwfurtado.ifly.airports.features.gates;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import me.github.fwfurtado.ifly.airports.domain.Airport;
import me.github.fwfurtado.ifly.airports.domain.Location;
import me.github.fwfurtado.ifly.airports.infra.GlobalExceptionHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class GateContractTest {

    private static final String  CGH = "CGH";
    private static final String GRU = "GRU";
    private static final Location DEFAULT_LOCATION = new Location("SP", "Guarulhos", "Cumbica");
    private static final Airport GRU_AIRPORT = new Airport(GRU, DEFAULT_LOCATION);

    @Mock
    private AddGateRepository repository;

    @Spy
    private GateFormConverter converter;

    @InjectMocks
    private AddGateService service;


    @BeforeEach
    void setup() {

        lenient().when(repository.findByIdentifier(CGH)).thenReturn(Optional.empty());
        lenient().when(repository.findByIdentifier(GRU)).thenReturn(Optional.of(GRU_AIRPORT));

        var mvcBuilder = MockMvcBuilders.standaloneSetup(new AddGateController(service)).setControllerAdvice(new GlobalExceptionHandler());

        RestAssuredMockMvc.standaloneSetup(mvcBuilder);
    }

}