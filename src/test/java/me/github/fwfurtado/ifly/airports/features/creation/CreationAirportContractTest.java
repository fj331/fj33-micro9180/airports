package me.github.fwfurtado.ifly.airports.features.creation;

import me.github.fwfurtado.ifly.airports.AirportsApplication;
import me.github.fwfurtado.ifly.airports.domain.Location;
import me.github.fwfurtado.ifly.airports.features.creation.CreationAirportController.AirportForm;
import me.github.fwfurtado.ifly.airports.features.gates.AddGateRepository;
import me.github.fwfurtado.ifly.airports.features.search.SearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;

@MockBeans({
        @MockBean(AddGateRepository.class),
        @MockBean(CreateAirportRepository.class),
        @MockBean(SearchRepository.class)
})
@ImportAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
@SpringBootTest(classes = AirportsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureMessageVerifier
public class CreationAirportContractTest {

    @Autowired
    private CreationAirportService service;


    public void createAirport()  {

        var form = new AirportForm();
        var congonhas = new Location("SP", "São Paulo", "Vila Congonhas");
        form.setCode("cgh");
        form.setLocation(congonhas);


        service.createNewAirportFrom(form);

    }
}