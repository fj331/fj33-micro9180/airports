import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        url "/airports/cgh/gates"
        method PUT()

        headers {
            contentType applicationJson()
        }

        body([
                "gate":"D101"
        ])
    }

    response {
        status NOT_FOUND()
    }
}