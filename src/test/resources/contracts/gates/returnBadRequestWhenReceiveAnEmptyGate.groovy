import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        url "/airports/cnf/gates"
        method PUT()

        headers {
            contentType applicationJson()
        }

        body([
                "gate":""
        ])
    }

    response {
        status BAD_REQUEST()
    }
}