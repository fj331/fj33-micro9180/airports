package contracts.gates

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        url "/airports/gru/gates"
        method PUT()

        headers {
            contentType applicationJson()
        }

        body([
              "gate": "D101"
        ])

    }

    response {
        status ACCEPTED()
    }
}