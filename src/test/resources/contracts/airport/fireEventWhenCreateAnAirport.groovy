import org.springframework.cloud.contract.spec.Contract

Contract.make {
    label 'create-airport'

    input {
        triggeredBy('createAirport()')
    }

    outputMessage {
        sentTo('airports')
        body([
                time: value(producer(regex("([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])\\.\\d+")),consumer("2020-04-09T20:28:30.123")),
                identifier: 'CGH',
                location: [
                        state: 'SP',
                        city: 'São Paulo',
                        neighborhood: 'Vila Congonhas'
                ]
        ])
    }
}